const app = Vue.createApp({
    data() {
        return {
            friends: [
                {
                    id: 'benjo',
                    name: 'Mikic Benjamin',
                    phone: '0038761997311',
                    email: 'mikicbenjamin92@gmail.com'
                },
                {
                    id: 'bero',
                    name: 'Sapina Berislav',
                    phone: '0038763993940',
                    email: 'bero.92@gmail.com'
                },
            ]
        }
    }
});

app.component('friend-contacts', {
    template: `
    <li>
    <h2>{{ friend.name }}</h2>
    <button @click="toggleDetails">{{ detailsAreVisible ? "Hide" : "Show"}} Details</button>
    <ul v-if="detailsAreVisible">
      <li><strong>Phone:</strong>{{ friend.phone }}</li>
      <li><strong>Email:</strong>{{ friend.email  }}</li>
    </ul>
  </li>
    `,
    data() {
        return {
            detailsAreVisible: false,
            friend: {
                id: 'bero',
                name: 'Sapina Berislav',
                phone: '0038763993940',
                email: 'bero.92@gmail.com'
            }
        }
    },
    methods: {
        toggleDetails() {
            this.detailsAreVisible = !this.detailsAreVisible;
        }
    }
})

app.mount('#app')