Vue.createApp({
    data() {
        return {
            boxAselected: false,
            boxBselected: false,
            boxCselected: false
        }
    },
    methods: {
        boxSelected(box) {
            if (box === 'A') {
                this.boxAselected =  !this.boxAselected;
            } else if(box === 'B') {
                this.boxBselected = !this.boxBselected;
            } else if (box === 'C') {
                this.boxCselected = !this.boxCselected;
            }
        }
    },
    computed: {
        // use only on larger sites :D
        boxAClasses() {
            return {active: this.boxCselected}
        }
    }
}).mount("#styling")