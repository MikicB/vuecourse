const app = Vue.createApp({
    data() {
        return {
            inputClass: '',
            paraIsVisible: true,
            inputBackgroundColor: ''
        }
    },
    methods: {
        toggleBtn() {
            this.paraIsVisible = !this.paraIsVisible
        }
    },
    computed: {
        paraClasses() {
            return {
                user1: this.inputClass == 'user1',
                user2: this.inputClass == 'user2',
                visible: this.paraIsVisible,
                hidden: !this.paraIsVisible
            }
        }
    }
}).mount("#assignment")