Vue.createApp({
    data() {
        return { 
            enteredGoal: "",
            items: [],
            itemsAreVisible: true
        }
    },
    methods: {
        addGoal() {
            this.items.push(this.enteredGoal)
        },
        hiddenShowen() {
            this.itemsAreVisible = !this.itemsAreVisible
        }
    }
}).mount('#assignment') 