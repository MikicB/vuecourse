function getRandomDmg(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

const app = Vue.createApp({
    data() {
        return {
            playerHealt: 100,
            monsterHealt: 100,
            currentRound: 0,
            winner: null,
            logMessages: []
        }
    },
    watch: {
        playerHealt(value) {
            if (value <= 0 && this.monsterHealt <= 0) {
                // Draw  
                this.winner = "draw"
            }else if (value <= 0) {
                // Player Lost
                this.winner = "monster"
            }
        },
        monsterHealt(value) {
            if (value <= 0 && this.playerHealt <= 0) {
                // Draw                
                this.winner = "draw"
            }else if (value <= 0) {
                // Monster Lost
                this.winner = "player"
            }
        }
    },
    computed: {
        monsterBarStyle() {
            if (this.monsterHealt < 0) {
                return { width: '0%' }
            }
            return {width: this.monsterHealt + '%'}
        },
        playerBarStyle() {
            if (this.playerHealt < 0) {
                return { width: '0%' }
            }
            return {width: this.playerHealt + '%'}
        },
        mayUseSpecialAttack() {
            return this.currentRound % 3 !== 0;
        }
    },
    methods: {
        startGame() {
            this.playerHealt = 100;
            this.monsterHealt = 100; 
            this.winner = null;
            this.currentRound = 0;
            this.logMessages = [];
        },
        attackMonster() {
            this.currentRound++
            const damage = getRandomDmg(5, 12);
            this.monsterHealt -= damage;
            this.addLogMsg('player', 'attack', damage)
            this.attackPlayer();
        },
        attackPlayer() {
            const damage = getRandomDmg(8, 15);
            this.playerHealt -= damage;
            this.addLogMsg('monster', 'attack', damage)
        },
        specialAttackMonster() {
            this.currentRound++;
            const damage = getRandomDmg(10, 20);
            this.monsterHealt -= damage;
            this.addLogMsg('player', 'attack', damage)            
            this.attackPlayer();
        },
        healPlayer() {
            this.currentRound++;
            const damage = getRandomDmg(8, 20);
            if (this.playerHealt + damage > 100) {
                this.playerHealt = 100;
            }else{
                this.playerHealt += damage;
            }
            this.addLogMsg('player', 'heal', this.damage);            
            this.attackPlayer();
        },
        surrender() {
            this.winner = 'monster';
        },
        addLogMsg(who, what, value) {
            this.logMessages.unshift({
                actionBy: who,
                actionType: what,
                actionValue: value
            });
        },
    }
})
app.mount("#game")