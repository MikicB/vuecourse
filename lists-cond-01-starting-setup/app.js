const app = Vue.createApp({
  data() {
    return {
       goals: [],
       enteredGoalValue: ''
      };
  },
  methods: { 
    addGoal() {
      this.goals.push(this.enteredGoalValue);
    }, 
    removeGoals(index) {
      this.goals.splice(index, 1)
    }
  },
  computed: {

  }
});

app.mount('#user-goals');
