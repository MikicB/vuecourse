const app = Vue.createApp({
    data() {
        return {
            counter: 0
        }
    },
    methods: {
        addValue5(num) {
            this.counter = this.counter + num
        },
        addValue1(num) {
            this.counter = this.counter + num
        }
    },
    computed: {
        result() {
            if (this.counter < 37) {
               return "Not there yet"
            }else if(this.counter === 37) {
                return this.counter
            }else {
                return "Too much!"
            }
        }
    },
    watch: {
        result() {
            const that = this
            setTimeout(function() {
                that.counter = 0;
            },5000)
        }
    }
}).mount('#assignment')